<?php
declare(strict_types=1);

namespace PHPSTORM_META {

    use Zend\ServiceManager\ServiceManager;

    override(
        ServiceManager::get(0),
        type(0)
    );
}
