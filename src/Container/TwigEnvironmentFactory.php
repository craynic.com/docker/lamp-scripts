<?php
declare(strict_types=1);

namespace App\Container;

use App\TwigLoader;
use Twig\Environment;

final class TwigEnvironmentFactory
{
    public function __invoke(): Environment
    {
        return new Environment(
            new TwigLoader('tpl'),
            [
                'autoescape' => false,
            ]
        );
    }
}
