FROM php:7.4-cli-alpine AS builder

COPY . /opt/app/
WORKDIR /opt/app

RUN apk add --no-cache composer~=1 \
    && composer install --no-dev \
    && php -dphar.readonly=0 compile.php -f app.phar

FROM php:7.4-cli-alpine

# copy the app built in the previous step
COPY --from=builder /opt/app/app.phar /usr/local/sbin/realm-admin

# mark the app as executable
RUN chmod 0755 /usr/local/sbin/realm-admin

# set the app as a default command
ENTRYPOINT ["realm-admin"]