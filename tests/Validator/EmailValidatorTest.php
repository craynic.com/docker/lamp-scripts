<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\EmailValidator;
use App\Validator\Exception\InvalidEmail;
use App\Validator\Validator;

final class EmailValidatorTest extends ValidatorTest
{
    public function getValidator(): Validator
    {
        return new EmailValidator();
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return InvalidEmail::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['user.name@example.com'],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            ['user.name@8.8.8.8'],
            ['asdf'],
            ['user.name'],
            ['test@best'],
            [null],
        ];
    }
}
