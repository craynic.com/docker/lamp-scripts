<?php
declare(strict_types=1);

namespace Tests\App\Command\Php;

use App\Command\Command;
use App\Command\Php\GeneratePoolConf;
use App\TmpDirProvider;
use App\Validator\Exception\InvalidGroupName;
use App\Validator\Exception\InvalidHostname;
use App\Validator\Exception\InvalidInteger;
use App\Validator\Exception\InvalidPhpVersion;
use App\Validator\Exception\InvalidUserName;
use App\Validator\Exception\NonExistentDir;
use Symfony\Component\Filesystem\Filesystem;
use Tests\App\Command\CommandTestCase;

final class GeneratePoolConfTest extends CommandTestCase
{
    private string $tmpDir;

    private array $defaultArguments;

    protected function setUp(): void
    {
        parent::setUp();

        $tmpDirProvider = $this->container->get(TmpDirProvider::class);
        $this->tmpDir = $tmpDirProvider->create();

        $this->defaultArguments = [
            'host-name' => 'example.com',
            'owner-user' => 'example_user',
            'owner-group' => 'example_group',
            '--home-dir' => $this->tmpDir,
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->container->get(Filesystem::class)->remove($this->tmpDir);
    }

    protected function getCommandToTest(): Command
    {
        return $this->container->get(GeneratePoolConf::class);
    }

    public function testItWorksWithDefaults(): void
    {
        $this->commandTester->execute($this->defaultArguments);

        $generatedConf = $this->commandTester->getDisplay();

        $this->assertRegExp('|^\[example\.com\]$|m', $generatedConf);
        $this->assertRegExp('|^user\s*=\s*example_user$|m', $generatedConf);
        $this->assertRegExp('|^group\s*=\s*example_group$|m', $generatedConf);
    }

    public function testItWorksWithAllParameters(): void
    {
        $this->commandTester->execute([
            'php-version' => '5.6',
            '--request-timeout' => 300,
            '--max-processes' => 100,
            '--memory-limit' => 128,
        ] + $this->defaultArguments);

        $generatedConf = $this->commandTester->getDisplay();

        $this->assertRegExp('|^\[example\.com\]$|m', $generatedConf);
        $this->assertRegExp('|^user\s*=\s*example_user$|m', $generatedConf);
        $this->assertRegExp('|^group\s*=\s*example_group$|m', $generatedConf);
        $this->assertRegExp('|^request_terminate_timeout\s*=\s*300$|m', $generatedConf);
        $this->assertRegExp('|^listen\s*=\s*php-fpm-5\.6\.sock$|m', $generatedConf);
        $this->assertRegExp('|^pm\.max_children\s*=\s*100$|m', $generatedConf);
        $this->assertRegExp('|^php_admin_value\[max_execution_time\]\s*=\s*300$|m', $generatedConf);
        $this->assertRegExp('|^php_admin_value\[max_input_time\]\s*=\s*300$|m', $generatedConf);
        $this->assertRegExp('|^php_admin_value\[memory_limit\]\s*=\s*128M$|m', $generatedConf);
    }

    public function testInvalidHostNameThrowsException(): void
    {
        $this->expectException(InvalidHostname::class);
        $this->expectExceptionMessage('asdf.');

        $this->commandTester->execute([
            'host-name' => 'asdf.',
        ] + $this->defaultArguments);
    }

    public function testInvalidUserNameThrowsException(): void
    {
        $this->expectException(InvalidUserName::class);
        $this->expectExceptionMessage('example_user_');

        $this->commandTester->execute([
            'owner-user' => 'example_user_',
        ] + $this->defaultArguments);
    }

    public function testInvalidGroupNameThrowsException(): void
    {
        $this->expectException(InvalidGroupName::class);
        $this->expectExceptionMessage('example_group_');

        $this->commandTester->execute([
            'owner-group' => 'example_group_',
        ] + $this->defaultArguments);
    }

    public function testInvalidPhpVersionThrowsException(): void
    {
        $this->expectException(InvalidPhpVersion::class);
        $this->expectExceptionMessage('4.3');

        $this->commandTester->execute([
            'php-version' => '4.3',
        ] + $this->defaultArguments);
    }

    public function testInvalidRequestTimeoutThrowsException(): void
    {
        $this->expectException(InvalidInteger::class);
        $this->expectExceptionMessage('1000');

        $this->commandTester->execute([
            '--request-timeout' => '1000',
        ] + $this->defaultArguments);
    }

    public function testInvalidMaxProcessesThrowsException(): void
    {
        $this->expectException(InvalidInteger::class);
        $this->expectExceptionMessage('1000');

        $this->commandTester->execute([
            '--max-processes' => '1000',
        ] + $this->defaultArguments);
    }

    public function testInvalidMemoryLimitThrowsException(): void
    {
        $this->expectException(InvalidInteger::class);
        $this->expectExceptionMessage('10240');

        $this->commandTester->execute([
            '--memory-limit' => '10240',
        ] + $this->defaultArguments);
    }

    public function testInvalidHomedirThrowsException(): void
    {
        $this->expectException(NonExistentDir::class);
        $this->expectExceptionMessage('/var/www/example.com');

        $this->commandTester->execute([
            '--home-dir' => '/var/www/example.com',
        ] + $this->defaultArguments);
    }
}
