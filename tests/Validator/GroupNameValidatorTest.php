<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\Exception\InvalidGroupName;
use App\Validator\GroupNameValidator;
use App\Validator\Validator;

final class GroupNameValidatorTest extends ValidatorTest
{
    public function getValidator(): Validator
    {
        return new GroupNameValidator();
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return InvalidGroupName::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['group'],
            ['group_group'],
            ['group-group'],
            ['g123'],
            ['g'],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            ['group_'],
            ['1g'],
            ['_asdf'],
            [''],
            ['group.name'],
            [str_repeat('g', 33)],
            [null],
        ];
    }
}
