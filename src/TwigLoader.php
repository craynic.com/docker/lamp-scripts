<?php
declare(strict_types=1);

namespace App;

use Throwable;
use Twig\Error\LoaderError;
use Twig\Loader\LoaderInterface;
use Twig\Source;

final class TwigLoader implements LoaderInterface
{
    private string $dir;

    public function __construct(string $dir)
    {
        $this->dir = $dir;
    }

    public function getSourceContext($name): Source
    {
        return new Source(
            $this->getTemplate($name),
            $name
        );
    }

    public function exists($name): bool
    {
        try {
            $this->getTemplate($name);

            return true;
        } catch (Throwable $exception) {
            return false;
        }
    }

    public function getCacheKey($name): string
    {
        return $name;
    }

    public function isFresh($name, $time): bool
    {
        return true;
    }

    protected function getTemplate(string $name): string
    {
        $fullPath = sprintf('%s/%s', $this->dir, $name);

        if (!is_readable($fullPath)) {
            throw new LoaderError(
                sprintf('Template "%s" does not exist.', $name)
            );
        }

        return file_get_contents($fullPath);
    }
}
