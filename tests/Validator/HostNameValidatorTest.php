<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\Exception\InvalidHostname;
use App\Validator\HostNameValidator;
use App\Validator\Validator;

final class HostNameValidatorTest extends ValidatorTest
{
    public function getValidator(): Validator
    {
        return new HostNameValidator();
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return InvalidHostname::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['localhost'],
            ['localhost.localdomain'],
            ['www.example.com'],
            ['8.8.8.8'],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            ['asdf.'],
            ['asdf..sdf'],
            [null],
        ];
    }
}
