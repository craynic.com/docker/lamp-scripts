<?php
declare(strict_types=1);

namespace Tests\App;

use Laminas\Config\Factory;
use Laminas\ServiceManager\ServiceManager;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    protected ServiceManager $container;

    protected function setUp(): void
    {
        parent::setUp();

        // read config
        $config = Factory::fromFile('config/default.php', true);

        // define DI-container
        $this->container = new ServiceManager();
        $this->container->configure($config->toArray());
        $this->container->setService('config', $config);
    }
}
