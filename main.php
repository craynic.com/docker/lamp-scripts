<?php
require 'vendor/autoload.php';

use Laminas\Config\Factory;
use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Application;

Phar::interceptFileFuncs();

// read config
$config = Factory::fromFile('config/default.php', true);

// define DI-container
$container = new ServiceManager();
$container->configure($config->toArray());
$container->setService('config', $config);

/** @noinspection PhpUnhandledExceptionInspection */
$container->get(Application::class)->run();
