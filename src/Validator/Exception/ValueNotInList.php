<?php
declare(strict_types=1);

namespace App\Validator\Exception;

use RuntimeException;

final class ValueNotInList extends RuntimeException
{
    public function __construct($value)
    {
        parent::__construct(
            sprintf(
                'Given value "%s" is not in the list of allowed values',
                (string) $value
            )
        );
    }
}
