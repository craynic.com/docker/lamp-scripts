<?php
declare(strict_types=1);

namespace Tests\App\Command;

use App\Command\Command;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\App\TestCase;

abstract class CommandTestCase extends TestCase
{
    /** @var CommandTester */
    protected $commandTester;

    protected function setUp(): void
    {
        parent::setUp();

        $application = $this->container->get(Application::class);
        $application->add($command = $this->getCommandToTest());
        $this->commandTester = new CommandTester($application->find($command::$defaultName));
    }

    abstract protected function getCommandToTest(): Command;
}
