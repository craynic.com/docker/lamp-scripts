<?php
declare(strict_types=1);

namespace App\Validator;

use App\Validator\Exception\InvalidInteger;

final class IntegerValidator implements Validator
{
    private int $min;

    private int $max;

    public function __construct(int $min = null, int $max = null)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function validate($value): void
    {
        if (!is_int($value) && !preg_match('|^\d+$|', (string) $value)) {
            throw new InvalidInteger($value);
        }

        if (null !== $this->min && $this->min > $value) {
            throw new InvalidInteger($value);
        }

        if (null !== $this->max && $this->max < $value) {
            throw new InvalidInteger($value);
        }
    }
}
