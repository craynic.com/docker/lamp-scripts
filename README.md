# Supporting Scripts for the [LAP Project](https://gitlab.com/craynic.com/docker/lap)

This project is part of the [LAP project](https://gitlab.com/craynic.com/docker/lap)
and provides scripts to manage hostings:

* Create home directories
* Generate configuration (Apache, PHP)
* &hellip;

The project is based on [Symfony's Console](https://symfony.com/doc/current/components/console.html)
and is distributed as a PHAR archive.

## Building the PHAR

```bash
./compile.php [-f|--filename FILENAME] [-o|--overwrite-existing]
```

The compilation requires the INI settings `phar.readonly = 0`.

One-line command to enable the settings and run the compilation:
```bash
php -d phar.readonly=0 compile.php [-f|--filename FILENAME] [-o|--overwrite-existing]
```

## Usage and Examples

To see usage details of the commands:

```bash
./app.phar help
```

&hellip;or during development using the non-compiled version:

```bash
php main.php help
```

To generate pool configuration for PHP:

```bash
./app.phar php:generate-pool-conf example.com user group
```

To generate vhost configuration file for Apache and save as `vhost.conf`:

```bash
./app.phar apache:generate-vhost-conf -f vhost.conf example.com
```
