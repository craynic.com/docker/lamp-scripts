<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\Exception\InvalidPhpVersion;
use App\Validator\PhpVersionValidator;
use App\Validator\Validator;

final class PhpVersionValidatorTest extends ValidatorTest
{
    public function getValidator(): Validator
    {
        return new PhpVersionValidator();
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return InvalidPhpVersion::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['5.6'],
            ['7.0'],
            ['7.1'],
            ['7.2'],
            ['7.3'],
            ['7.4'],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            ['7.5'],
            ['abcd'],
            [''],
            [null],
        ];
    }
}
