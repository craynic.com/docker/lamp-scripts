<?php
declare(strict_types=1);

namespace App\Validator;

final class ArrayValidator implements Validator
{
    private Validator $itemValidator;

    public function __construct(Validator $itemValidator)
    {
        $this->itemValidator = $itemValidator;
    }

    public function validate($value): void
    {
        $itemValidator = $this->itemValidator;
        array_map(
            static function ($value) use ($itemValidator): void {
                $itemValidator->validate($value);
            },
            $value
        );
    }
}
