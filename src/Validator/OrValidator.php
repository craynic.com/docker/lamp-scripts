<?php
declare(strict_types=1);

namespace App\Validator;

use Throwable;

final class OrValidator implements Validator
{
    /** @var Validator[] */
    private array $validators;

    public function __construct(Validator ...$validators)
    {
        $this->validators = $validators;
    }

    public function validate($value): void
    {
        $lastException = null;

        foreach ($this->validators as $validator) {
            try {
                $validator->validate($value);
                return;
            } catch (Throwable $exception) {
                $lastException = $exception;
            }
        }

        throw $lastException;
    }
}
