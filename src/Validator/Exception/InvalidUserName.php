<?php
declare(strict_types=1);

namespace App\Validator\Exception;

use RuntimeException;

final class InvalidUserName extends RuntimeException
{
    public function __construct($value)
    {
        parent::__construct(
            sprintf(
                'Invalid user name "%s"',
                (string) $value
            )
        );
    }
}
