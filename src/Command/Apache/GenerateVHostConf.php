<?php
declare(strict_types=1);

namespace App\Command\Apache;

use App\Command\GenerateConfAbstract;
use App\Validator\ArrayValidator;
use App\Validator\DirNameValidator;
use App\Validator\EmailValidator;
use App\Validator\HostNameValidator;
use App\Validator\OrValidator;
use App\Validator\PhpVersionValidator;
use App\Validator\StaticValueValidator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class GenerateVHostConf extends GenerateConfAbstract
{
    /** @var string */
    public static $defaultName = 'apache:generate-vhost-conf';

    protected function configure(): void
    {
        $this
            ->setDescription('Generates a new Apache VHost configuration file')
            ->addArgument('server-name', InputArgument::REQUIRED, 'Name of the VHost for ServerName directive')
            ->addOption(
                'server-aliases',
                ['a'],
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'Aliases for ServerAlias directive'
            )
            ->addOption(
                'server-admin',
                null,
                InputOption::VALUE_REQUIRED,
                'Admin\'s e-mail for ServerAdmin directive',
                'support@realm.cz'
            )
            ->addOption(
                'php-version',
                null,
                InputOption::VALUE_REQUIRED,
                'Version of PHP to use (e.g. 5.6, 7.4)',
                '7.4'
            )
            ->addOption(
                'home-dir',
                null,
                InputOption::VALUE_REQUIRED,
                'Home directory of the VHost, defaults to /var/www/%ServerName%'
            )
        ;

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $serverName = $input->getArgument('server-name');
        $serverAliases = $input->getOption('server-aliases');
        $serverAdmin = $input->getOption('server-admin');
        $homeDir = rtrim((string) $input->getOption('home-dir')) ?: ('/var/www/' . $serverName);
        $phpVersion = $input->getOption('php-version');

        (new HostNameValidator())->validate($serverName);
        (new ArrayValidator(new OrValidator(new StaticValueValidator('*'), new HostNameValidator())))->validate($serverAliases);
        (new EmailValidator())->validate($serverAdmin);
        (new DirNameValidator())->validate($homeDir);
        (new PhpVersionValidator())->validate($phpVersion);

        $this->outputGeneratedConfig(
            $input,
            $output,
            $this->twig->load('apache/vhost.conf.twig')->render([
                'serverName' => $serverName,
                'serverAliases' => $serverAliases,
                'serverAdmin' => $serverAdmin,
                'homeDir' => $homeDir,
                'phpVersion' => $input->getOption('php-version'),
            ])
        );

        return 0;
    }
}
