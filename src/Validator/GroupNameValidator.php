<?php
declare(strict_types=1);

namespace App\Validator;

use App\Validator\Exception\InvalidGroupName;

final class GroupNameValidator implements Validator
{
    public function validate($value): void
    {
        if (!is_string($value) || !preg_match('|^[a-z](?:[\w\-]{0,30}[a-z0-9])?$|i', $value)) {
            throw new InvalidGroupName($value);
        }
    }
}
