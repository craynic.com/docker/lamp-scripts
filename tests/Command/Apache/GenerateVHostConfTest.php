<?php
declare(strict_types=1);

namespace Tests\App\Command\Php;

use App\Command\Apache\GenerateVHostConf;
use App\Command\Command;
use App\TmpDirProvider;
use App\Validator\Exception\InvalidEmail;
use App\Validator\Exception\InvalidHostname;
use App\Validator\Exception\InvalidPhpVersion;
use App\Validator\Exception\NonExistentDir;
use Symfony\Component\Filesystem\Filesystem;
use Tests\App\Command\CommandTestCase;

final class GenerateVHostConfTest extends CommandTestCase
{
    private string $tmpDir;

    private array $defaultArguments;

    protected function setUp(): void
    {
        parent::setUp();

        $tmpDirProvider = $this->container->get(TmpDirProvider::class);
        $this->tmpDir = $tmpDirProvider->create();

        $this->defaultArguments = [
            'server-name' => 'example.com',
            '--home-dir' => $this->tmpDir,
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->container->get(Filesystem::class)->remove($this->tmpDir);
    }

    protected function getCommandToTest(): Command
    {
        return $this->container->get(GenerateVHostConf::class);
    }

    public function testItWorksWithDefaults(): void
    {
        $this->commandTester->execute($this->defaultArguments);

        $generatedConf = $this->commandTester->getDisplay();

        $this->assertRegExp('|^\s*ServerName example\.com\s*$|m', $generatedConf);
    }

    public function testItWorksWithAllParameters(): void
    {
        $this->commandTester->execute([
            '--server-aliases' => ['example2.org', 'example3.net'],
            '--server-admin' => 'admin@example.com',
            '--php-version' => '5.6',
        ] + $this->defaultArguments);

        $generatedConf = $this->commandTester->getDisplay();

        $this->assertRegExp('|ServerName example\.com|m', $generatedConf);
        $this->assertRegExp('|ServerAlias example2\.org example3\.net|m', $generatedConf);
        $this->assertRegExp('|ServerAdmin admin@example\.com|m', $generatedConf);
        $this->assertRegExp('|' . preg_quote($this->tmpDir, '|') . '/php-fpm-5\.6\.sock|m', $generatedConf);
    }

    public function testItWorksWithAsteriskAlias(): void
    {
        $this->commandTester->execute([
            '--server-aliases' => ['*'],
        ] + $this->defaultArguments);

        $generatedConf = $this->commandTester->getDisplay();

        $this->assertRegExp('|^\s*ServerAlias \*\s*$|m', $generatedConf);
    }

    public function testInvalidHostNameThrowsException(): void
    {
        $this->expectException(InvalidHostname::class);
        $this->expectExceptionMessage('invalid:hostname');

        $this->commandTester->execute([
            'server-name' => 'invalid:hostname',
        ] + $this->defaultArguments);
    }

    public function testInvalidAliasHostNameThrowsException(): void
    {
        $this->expectException(InvalidHostname::class);
        $this->expectExceptionMessage('invalid:hostname');

        $this->commandTester->execute([
            '--server-aliases' => ['example2.org', 'invalid:hostname'],
        ] + $this->defaultArguments);
    }

    public function testInvalidServerAdminEmailThrowsException(): void
    {
        $this->expectException(InvalidEmail::class);
        $this->expectExceptionMessage('test@best');

        $this->commandTester->execute([
            '--server-admin' => 'test@best',
        ] + $this->defaultArguments);
    }

    public function testInvalidHomedirThrowsException(): void
    {
        $this->expectException(NonExistentDir::class);
        $this->expectExceptionMessage('/var/www/example.com');

        $this->commandTester->execute([
            '--home-dir' => '/var/www/example.com',
        ] + $this->defaultArguments);
    }

    public function testInvalidPhpVersionThrowsException(): void
    {
        $this->expectException(InvalidPhpVersion::class);
        $this->expectExceptionMessage('4.3');

        $this->commandTester->execute([
            '--php-version' => '4.3',
        ] + $this->defaultArguments);
    }
}
