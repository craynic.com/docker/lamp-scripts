<?php
declare(strict_types=1);

namespace App\Command\VHost;

use App\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Create extends Command
{
    /** @var string */
    public static $defaultName = 'vhost:create';

    protected function configure(): void
    {
        // TODO:
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // TODO:
        return 0;
    }
}
