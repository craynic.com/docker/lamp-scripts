<?php
declare(strict_types=1);

namespace App;

use RuntimeException;

final class TmpDirProvider
{
    public function generateName(): string
    {
        $tmpDir = sys_get_temp_dir() . '/' . uniqid('', true);

        if (is_file($tmpDir) || is_dir($tmpDir)) {
            throw new RuntimeException(sprintf('Directory "%s" already exists', $tmpDir));
        }

        return $tmpDir;
    }

    public function create(): string
    {
        $tmpDir = $this->generateName();

        if (!mkdir($tmpDir) && !is_dir($tmpDir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $tmpDir));
        }

        return $tmpDir;
    }
}
