<?php
declare(strict_types=1);

namespace App\Validator\Exception;

use RuntimeException;

final class NonExistentDir extends RuntimeException
{
    public function __construct($value)
    {
        parent::__construct(
            sprintf(
                'Directory "%s" does not exist',
                (string) $value
            )
        );
    }
}
