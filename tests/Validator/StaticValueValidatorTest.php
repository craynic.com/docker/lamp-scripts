<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\Exception\ValueNotInList;
use App\Validator\StaticValueValidator;
use App\Validator\Validator;

final class StaticValueValidatorTest extends ValidatorTest
{
    public function getValidator(): Validator
    {
        return new StaticValueValidator('foo', 'bar', 3, null, false, true);
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return ValueNotInList::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['foo'],
            ['bar'],
            [3],
            [null],
            [false],
            [true],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            ['foobar'],
        ];
    }
}
