<?php
declare(strict_types=1);

namespace App\Validator;

use App\Validator\Exception\InvalidHostname;

final class HostNameValidator implements Validator
{
    public function validate($value): void
    {
        if (!is_string($value) || !preg_match('|^(?:[\w\-]+\.)*[\w\-]+$|', $value)) {
            throw new InvalidHostname($value);
        }
    }
}
