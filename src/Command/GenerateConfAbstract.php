<?php
declare(strict_types=1);

namespace App\Command;

use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Twig\Environment;

abstract class GenerateConfAbstract extends Command
{
    protected Environment $twig;

    public function __construct(Environment $twig)
    {
        parent::__construct();

        $this->twig = $twig;
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'filename',
                ['f'],
                InputOption::VALUE_REQUIRED,
                'Optional filename to store the configuration to'
            )
            ->addOption(
                'overwrite-existing',
                ['o'],
                InputOption::VALUE_NONE,
                'Overwrite existing configuration file if it exists'
            )
        ;

        parent::configure();
    }

    protected function outputGeneratedConfig(InputInterface $input, OutputInterface $output, string $config): void
    {
        $fileName = $input->getOption('filename');
        $overwrite = (bool)$input->getOption('overwrite-existing');

        // no filename, send the contents to the output
        if (null === $fileName) {
            $output->write($config);
            return;
        }

        if (file_exists($fileName)) {
            if (!$overwrite) {
                throw new RuntimeException(
                    sprintf('The file "%s" already exists, add "--overwrite" to force.', $fileName)
                );
            }

            if (!unlink($fileName)) {
                throw new RuntimeException(
                    sprintf('Error removing the existing file "%s".', $fileName)
                );
            }
        }

        if (false === file_put_contents($fileName, $config)) {
            throw new RuntimeException(
                sprintf('Error writing the configuration to the file "%s".', $fileName)
            );
        }

        $output->writeln(
            sprintf('The configuration was written to file "%s".', $fileName)
        );
    }
}
