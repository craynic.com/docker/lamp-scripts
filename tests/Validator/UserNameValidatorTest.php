<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\Exception\InvalidUserName;
use App\Validator\UserNameValidator;
use App\Validator\Validator;

final class UserNameValidatorTest extends ValidatorTest
{
    public function getValidator(): Validator
    {
        return new UserNameValidator();
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return InvalidUserName::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['user'],
            ['user_user'],
            ['user-user'],
            ['u123'],
            ['u'],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            ['user_'],
            ['1u'],
            ['_asdf'],
            [''],
            ['user.name'],
            [str_repeat('u', 33)],
            [null],
        ];
    }
}
