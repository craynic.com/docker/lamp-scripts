<?php
declare(strict_types=1);

namespace App\Command;

abstract class Command extends \Symfony\Component\Console\Command\Command
{
    /** @var string */
    public static $defaultName;
}
