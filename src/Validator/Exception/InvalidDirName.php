<?php
declare(strict_types=1);

namespace App\Validator\Exception;

use RuntimeException;

final class InvalidDirName extends RuntimeException
{
    public function __construct($value)
    {
        parent::__construct(
            sprintf(
                'Invalid directory name "%s"',
                (string) $value
            )
        );
    }
}
