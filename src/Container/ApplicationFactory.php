<?php
declare(strict_types=1);

namespace App\Container;

use Generator;
use Interop\Container\ContainerInterface;
use ReflectionClass;
use Symfony\Component\Console\Application;

final class ApplicationFactory
{
    public function __invoke(ContainerInterface $container): Application
    {
        $application = new Application();

        $this->registerCommands($container, $application);

        return $application;
    }

    private function registerCommands(ContainerInterface $container, Application $application): void
    {
        foreach ($this->getAllFiles('src/Command') as $fileName) {
            $className = $this->convertFileNameToClassName($fileName);

            $reflectionClass = new ReflectionClass($className);
            if ($reflectionClass->isAbstract() || $reflectionClass->isInterface()) {
                continue;
            }

            $application->add(
                $container->get($className)
            );
        }
    }

    private function getAllFiles(string $dirName): Generator
    {
        $dir = opendir($dirName);

        while (($file = readdir($dir)) !== false) {
            if (in_array($file, ['.', '..'], true)) {
                continue;
            }

            $fullPath = $dirName . '/' . $file;

            if (is_dir($fullPath)) {
                yield from $this->getAllFiles($fullPath);
            } else {
                yield $fullPath;
            }
        }

        closedir($dir);
    }

    private function convertFileNameToClassName(string $fileName): string
    {
        return 'App\\' . strtr(substr($fileName, 4, -4), ['/' => '\\']);
    }
}
