<?php
declare(strict_types=1);

namespace App\Command\Php;

use App\Command\GenerateConfAbstract;
use App\Validator\DirNameValidator;
use App\Validator\GroupNameValidator;
use App\Validator\HostNameValidator;
use App\Validator\IntegerValidator;
use App\Validator\PhpVersionValidator;
use App\Validator\UserNameValidator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class GeneratePoolConf extends GenerateConfAbstract
{
    private const MAX_REQUEST_LENGTH = 300;
    private const MAX_MAX_PROCESSES = 100;
    private const MAX_MEMORY_LIMIT = 1024;

    /** @var string */
    public static $defaultName = 'php:generate-pool-conf';

    protected function configure(): void
    {
        $this
            ->setDescription('Generates a new Apache VHost configuration file')
            ->addArgument('host-name', InputArgument::REQUIRED)
            ->addArgument('owner-user', InputArgument::REQUIRED)
            ->addArgument('owner-group', InputArgument::REQUIRED)
            ->addArgument('php-version', InputArgument::OPTIONAL, '', '7.4')
            ->addOption('request-timeout', ['t'], InputOption::VALUE_REQUIRED, '', 30)
            ->addOption('max-processes', ['p'], InputOption::VALUE_REQUIRED, '', 40)
            ->addOption('memory-limit', ['m'], InputOption::VALUE_REQUIRED, '', 32)
            ->addOption(
                'home-dir',
                null,
                InputOption::VALUE_REQUIRED,
                'Home directory of the VHost, defaults to /var/www/%HostName%'
            )
        ;

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $hostName = $input->getArgument('host-name');
        $ownerUser = $input->getArgument('owner-user');
        $ownerGroup = $input->getArgument('owner-group');
        $phpVersion = $input->getArgument('php-version');
        $requestTimeout = $input->getOption('request-timeout');
        $maxProcesses = $input->getOption('max-processes');
        $memoryLimit = $input->getOption('memory-limit');
        $homeDir = rtrim((string) $input->getOption('home-dir')) ?: ('/var/www/' . $hostName);

        (new HostNameValidator())->validate($hostName);
        (new UserNameValidator())->validate($ownerUser);
        (new GroupNameValidator())->validate($ownerGroup);
        (new PhpVersionValidator())->validate($phpVersion);
        (new IntegerValidator(1, self::MAX_REQUEST_LENGTH))->validate($requestTimeout);
        (new IntegerValidator(1, self::MAX_MAX_PROCESSES))->validate($maxProcesses);
        (new IntegerValidator(1, self::MAX_MEMORY_LIMIT))->validate($memoryLimit);
        (new DirNameValidator())->validate($homeDir);

        $this->outputGeneratedConfig(
            $input,
            $output,
            $this->twig->load('php/pool.conf.twig')->render([
                'hostName' => $hostName,
                'homeDir' => $homeDir,
                'ownerUser' => $ownerUser,
                'ownerGroup' => $ownerGroup,
                'phpVersion' => $phpVersion,
                'requestTimeout' => (int) $requestTimeout,
                'maxProcesses' => (int) $maxProcesses,
                'memoryLimit' => (int) $memoryLimit,
            ])
        );

        return 0;
    }
}
