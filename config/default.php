<?php

use App\Container\ApplicationFactory;
use App\Container\TwigEnvironmentFactory;
use Laminas\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Symfony\Component\Console\Application;
use Twig\Environment;

return [
    'factories' => [
        Application::class => ApplicationFactory::class,
        Environment::class => TwigEnvironmentFactory::class,
    ],
    'abstract_factories' => [
        ReflectionBasedAbstractFactory::class,
    ],
];
