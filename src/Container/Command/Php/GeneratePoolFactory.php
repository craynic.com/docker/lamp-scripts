<?php
declare(strict_types=1);

namespace App\Container\Command\Php;

use App\Command\Php\GeneratePoolConf;
use Interop\Container\ContainerInterface;
use Twig\Environment;

final class GeneratePoolFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new GeneratePoolConf(
            $container->get(Environment::class)
        );
    }
}
