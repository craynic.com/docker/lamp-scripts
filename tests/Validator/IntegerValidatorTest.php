<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\Exception\InvalidInteger;
use App\Validator\IntegerValidator;
use App\Validator\Validator;

final class IntegerValidatorTest extends ValidatorTest
{
    public function getValidator(): Validator
    {
        return new IntegerValidator(1, 1000);
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return InvalidInteger::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['123'],
            [123],
            [1],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            [123.2],
            [-1],
            ['adf'],
            [''],
            [null],
            [1001],
            [0],
        ];
    }
}
