<?php
declare(strict_types=1);

namespace App\Validator;

use App\Validator\Exception\InvalidDirName;
use App\Validator\Exception\NonExistentDir;

final class DirNameValidator implements Validator
{
    private bool $checkIfExists;

    public function __construct(bool $checkIfExists = true)
    {
        $this->checkIfExists = $checkIfExists;
    }

    public function validate($value): void
    {
        if (!is_string($value) || (strpos($value, "\0") !== false)) {
            throw new InvalidDirName($value);
        }

        if ($this->checkIfExists && (!file_exists($value) || !is_dir($value))) {
            throw new NonExistentDir($value);
        }
    }
}
