#!/usr/bin/env php
<?php
require 'vendor/autoload.php';

use App\TmpDirProvider;
use Laminas\Config\Factory;
use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

// read config
$config = Factory::fromFile(__DIR__ . '/config/default.php', true);

// define DI-container
$container = new ServiceManager();
$container->configure($config->toArray());
$container->setService('config', $config);

$filesystem = $container->get(Filesystem::class);
$tmpDirProvider = $container->get(TmpDirProvider::class);
$compileCommand = new class($filesystem, $tmpDirProvider, __DIR__) extends Command {
    private const FILENAME_BLACKLIST = [
        '.git',
        '.gitignore',
        '.gitlab-ci.yml',
        '.idea',
        '.php_cs.cache',
        '.phpstorm.meta.php',
        'compile.php',
        'composer.json',
        'composer.lock',
        'Dockerfile',
        'phpunit.xml',
        'README.md',
        'tests',
    ];

    /** @var string */
    public static $defaultName = 'compile';

    private Filesystem $filesystem;

    private TmpDirProvider $tmpDirProvider;

    private string $rootDir;

    public function __construct(Filesystem $filesystem, TmpDirProvider $tmpDirProvider, string $rootDir)
    {
        parent::__construct();

        $this->filesystem = $filesystem;
        $this->tmpDirProvider = $tmpDirProvider;
        $this->rootDir = $rootDir;
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'filename',
                ['f'],
                InputOption::VALUE_REQUIRED,
                'Optional filename to store the configuration to',
                'app.phar'
            )
            ->addOption(
                'overwrite-existing',
                ['o'],
                InputOption::VALUE_NONE,
                'Overwrite existing configuration file if it exists'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $fileName = $input->getOption('filename');
            $overwriteExisting = (bool) $input->getOption('overwrite-existing');

            // clean up
            $this->cleanUpOldFile($output, $fileName, $overwriteExisting);

            // create phar
            $output->writeln(sprintf('Creating PHAR file "%s"', $fileName));
            $phar = new Phar($fileName);

            $phar->startBuffering();
            $this->addFilesToPhar($output, $phar);
            $this->addPharStub($output, $phar);
            $phar->stopBuffering();

            $output->writeln(sprintf('Compressing PHAR file "%s"', $fileName));
            $phar->compressFiles(Phar::GZ);

            $output->writeln(sprintf('Setting executable flag for PHAR file "%s"', $fileName));
            $this->filesystem->chmod($fileName, 0770);

            $output->writeln(sprintf('PHAR file "%s" successfully created', $fileName));
        } catch (Throwable $exception) {
            fwrite(STDERR, $exception->getMessage() . PHP_EOL);

            return 1;
        }

        return 0;
    }

    private function cleanUpOldFile(OutputInterface $output, string $fileName, bool $overwrite): void
    {
        if (!file_exists($fileName)) {
            return;
        }

        if (!$overwrite) {
            throw new RuntimeException(
                sprintf('Filename "%s" already exists, add --overwrite to force overwriting.', $fileName)
            );
        }

        $output->writeln(sprintf('Cleaning up existing file "%s"', $fileName));
        unlink($fileName);
    }

    private function addFilesToPhar(OutputInterface $output, Phar $phar): void
    {
        $currentDir = rtrim($this->rootDir, '/') . '/';
        $fsIterator = new FilesystemIterator($currentDir);

        $tmpDir = $this->tmpDirProvider->create();
        $output->writeln(sprintf('Created temporary directory "%s"', $tmpDir));

        try {
            /** @noinspection PhpUndefinedClassConstantInspection */
            $blacklistedFiles = (array) static::FILENAME_BLACKLIST;

            /** @var SplFileInfo $fileInfo */
            foreach ($fsIterator as $fileInfo) {
                if (in_array($fileInfo->getFilename(), $blacklistedFiles, true)) {
                    $output->writeln(sprintf('Skipping blacklisted file "%s"', $fileInfo->getFilename()));
                    continue;
                }

                if ($fileInfo->isDir()) {
                    $output->writeln(sprintf('Adding directory "%s" to PHAR file', $fileInfo->getFilename()));
                    $this->filesystem->mirror(
                        $currentDir . $fileInfo->getFilename(),
                        $tmpDir . '/' . $fileInfo->getFilename(),
                        null,
                        ['override' => true, 'delete' => true]
                    );
                } else {
                    $output->writeln(sprintf('Adding file "%s" to PHAR file', $fileInfo->getFilename()));
                    $this->filesystem->copy(
                        $currentDir . $fileInfo->getFilename(),
                        $tmpDir . '/' . $fileInfo->getFilename(),
                        true
                    );
                }
            }

            $output->writeln(sprintf('Building PHAR file from temporary directory "%s"', $tmpDir));
            $phar->buildFromDirectory($tmpDir);
        } finally {
            $output->writeln(sprintf('Removing temporary directory "%s"', $tmpDir));
            $this->filesystem->remove($tmpDir);
        }
    }

    private function addPharStub(OutputInterface $output, Phar $phar): void
    {
        $output->writeln('Adding stub to PHAR file');
        $phar->setStub(
            sprintf(
                "%s\n%s",
                '#!/usr/bin/env php',
                $phar::createDefaultStub('main.php')
            )
        );
    }
};

$application = new Application();
$application->add($compileCommand);
$application->setDefaultCommand($compileCommand::$defaultName, true);

/** @noinspection PhpUnhandledExceptionInspection */
$application->run();
