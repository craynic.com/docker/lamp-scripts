<?php
declare(strict_types=1);

namespace App\Command\VHost;

use App\Command\Command;
use RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

final class CreateHomeDir extends Command
{
    /** @var string */
    public static $defaultName = 'vhost:create-homedir';

    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new Apache VHost homedir')
            ->addArgument('server-name', InputArgument::REQUIRED)
            ->addArgument('owner-user', InputArgument::REQUIRED)
            ->addArgument('owner-group', InputArgument::REQUIRED)
            ->addOption('apache-user', ['u'], InputOption::VALUE_REQUIRED, '', 'www-data')
            ->addOption('apache-group', ['g'], InputOption::VALUE_REQUIRED, '', 'www-data')
            ->addOption('document-root', ['d'], InputOption::VALUE_REQUIRED, '', '/var/www')
            ->addOption('force', ['f'], InputOption::VALUE_NONE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $hostName = $input->getArgument('server-name');
        $ownerUser = $input->getArgument('owner-user');
        $ownerGroup = $input->getArgument('owner-group');
        $apacheUser = $input->getOption('apache-user');
        $apacheGroup = $input->getOption('apache-group');
        $documentRoot = rtrim($input->getOption('document-root'), '/');
        $force = (bool) $input->getOption('force');
        $homeDir = sprintf('%s/%s', $documentRoot, $hostName);

        $filesystem = new Filesystem();
        if (!$force && $filesystem->exists($homeDir)) {
            throw new RuntimeException('Homedir already exists!');
        }

        # create home
        $filesystem->mkdir([
            $homeDir,
            "$homeDir/home",
            "$homeDir/htdocs",
            "$homeDir/logs",
            "$homeDir/tmp"
        ], 02750);
        $filesystem->touch([
            "$homeDir/logs/access.log",
            "$homeDir/logs/error.log",
            "$homeDir/logs/php-error.log",
        ]);

        // by default writable by apache and readable by user
        $this->chown([$homeDir], "$apacheUser:$ownerGroup");
        // htdocs is writable to user and readable by Apache
        $this->chown(["$homeDir/htdocs"], "$ownerUser:$apacheGroup");
        // php logs, home & tmp dir are writable only bu user
        $this->chown(["$homeDir/logs/php*", "$homeDir/home", "$homeDir/tmp"], "$ownerUser:$ownerGroup");

        // remove executable flags and "other"'s access for all files
        $this->chmod([$homeDir], 'a-x,o-rwx,ug+X');
        // set sticky bit for directories
        $this->setStickyBit($homeDir);

        return 0;
    }

    private function chown(array $files, string $user): void
    {
        $ret = null;
        system(
            sprintf('chown -R %1$s %2$s', $user, implode(' ', $files)),
            $ret
        );

        if (((int)$ret) === 0) {
            return;
        }

        throw new RuntimeException(
            sprintf(
                'Failed to change ownership to user "%1$s".',
                $user
            )
        );
    }

    private function chmod(array $files, string $permissions): void
    {
        $ret = null;
        system(
            sprintf(
                'chmod -R %1$s %2$s',
                $permissions,
                implode(' ', $files)
            ),
            $ret
        );

        if (((int)$ret) === 0) {
            return;
        }

        throw new RuntimeException(
            sprintf(
                'Failed to change permissions to "%1$s".',
                $permissions
            )
        );
    }

    private function setStickyBit(string $homeDir): void
    {
        $ret = null;
        system(
            sprintf(
                'find %1$s -type d | xargs chmod g+s',
                $homeDir
            ),
            $ret
        );

        if (((int)$ret) === 0) {
            return;
        }

        throw new RuntimeException(
            sprintf(
                'Failed to set sticky bit for "%1$s".',
                $homeDir
            )
        );
    }
}
