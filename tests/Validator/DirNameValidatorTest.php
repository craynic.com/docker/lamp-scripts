<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\TmpDirProvider;
use App\Validator\Exception\InvalidDirName;
use App\Validator\Exception\NonExistentDir;
use App\Validator\DirNameValidator;
use App\Validator\Validator;
use Symfony\Component\Filesystem\Filesystem;

final class DirNameValidatorTest extends ValidatorTest
{
    private string $existingTmpDir;

    private string $nonExistingTmpDir;

    protected function setUp(): void
    {
        parent::setUp();

        $tmpDirProvider = $this->container->get(TmpDirProvider::class);
        $this->existingTmpDir = $tmpDirProvider->create();
        $this->nonExistingTmpDir = $tmpDirProvider->generateName();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->container->get(Filesystem::class)->remove($this->existingTmpDir);
    }

    public function getValidator(): Validator
    {
        return new DirNameValidator(false);
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return InvalidDirName::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['this.might be @ any path / everything is - almost allowed in L*I*N*U*X systems'],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            ["Only files names with \0 are not allowed"],
            [null],
        ];
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testExistingDirectoryPassesTest(): void
    {
        $validator = new DirNameValidator(true);

        $validator->validate($this->existingTmpDir);
    }

    public function testNonExistingDirectoryThrowsException(): void
    {
        $validator = new DirNameValidator(true);

        $this->expectException(NonExistentDir::class);

        $validator->validate($this->nonExistingTmpDir);
    }
}
