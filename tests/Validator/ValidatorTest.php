<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\Validator;
use Tests\App\TestCase;

abstract class ValidatorTest extends TestCase
{
    abstract public function getValidator(): Validator;

    abstract public function getExpectedInvalidValueExceptionClass(): string;

    /**
     * @param mixed $stringToTest
     * @dataProvider validPatternsDataProvider
     * @doesNotPerformAssertions
     */
    public function testValidPatterns($stringToTest): void
    {
        $this->getValidator()->validate($stringToTest);
    }

    abstract public function validPatternsDataProvider(): array;

    /**
     * @param mixed $stringToTest
     * @dataProvider invalidPatternsDataProvider
     */
    public function testInvalidPatterns($stringToTest): void
    {
        $this->expectException($this->getExpectedInvalidValueExceptionClass());

        $this->getValidator()->validate($stringToTest);
    }

    abstract public function invalidPatternsDataProvider(): array;
}
