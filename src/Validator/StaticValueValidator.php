<?php
declare(strict_types=1);

namespace App\Validator;

use App\Validator\Exception\ValueNotInList;

final class StaticValueValidator implements Validator
{
    /** @var mixed[] */
    private array $possibleValues;

    public function __construct(...$possibleValues)
    {
        $this->possibleValues = $possibleValues;
    }

    public function validate($value): void
    {
        if (!in_array($value, $this->possibleValues, true)) {
            throw new ValueNotInList($value);
        }
    }
}
