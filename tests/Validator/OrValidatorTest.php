<?php
declare(strict_types=1);

namespace Tests\App\Validator;

use App\Validator\Exception\InvalidInteger;
use App\Validator\IntegerValidator;
use App\Validator\OrValidator;
use App\Validator\StaticValueValidator;
use App\Validator\Validator;

final class OrValidatorTest extends ValidatorTest
{
    public function getValidator(): Validator
    {
        return new OrValidator(
            new StaticValueValidator('foo'),
            new IntegerValidator(1, 10)
        );
    }

    public function getExpectedInvalidValueExceptionClass(): string
    {
        return InvalidInteger::class;
    }

    public function validPatternsDataProvider(): array
    {
        return [
            ['foo'],
            [3],
        ];
    }

    public function invalidPatternsDataProvider(): array
    {
        return [
            ['foobar'],
            [11],
            [null],
        ];
    }
}
