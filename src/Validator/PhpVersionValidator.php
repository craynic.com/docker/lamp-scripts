<?php
declare(strict_types=1);

namespace App\Validator;

use App\Validator\Exception\InvalidPhpVersion;

final class PhpVersionValidator implements Validator
{
    private const VALID_PHP_VERSIONS = ['5.6', '7.0', '7.1', '7.2', '7.3', '7.4'];

    public function validate($value): void
    {
        if (!in_array($value, self::VALID_PHP_VERSIONS, true)) {
            throw new InvalidPhpVersion($value);
        }
    }
}
