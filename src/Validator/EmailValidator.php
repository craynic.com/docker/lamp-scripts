<?php
declare(strict_types=1);

namespace App\Validator;

use App\Validator\Exception\InvalidEmail;

final class EmailValidator implements Validator
{
    public function validate($value): void
    {
        if (!is_string($value) || !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmail($value);
        }
    }
}
